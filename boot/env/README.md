Environment variables :
=======================

* SHL_ROOT : The Shelter's root path.
* SHL_PATH : The Shelter's system path for executables.

* SHL_OS : Tells on which operating system the Shelter is running.

Supported shells :
==================

sh (linux standart SHell) :
---------------------------

```sh
alias a2reload='service apache2 reload'

alias settle='/shl/sbin/deming settle'
alias commit='/shl/sbin/deming commit'
alias ship='/shl/sbin/deming ship'
alias devenv='/shl/sbin/deming env'
alias giting='/shl/sbin/deming sync'
alias deploy='/shl/sbin/deming deploy'

alias backendctl='supervisorctl -c /shl/var/lib/nebula/supervisor/daemon.cfg'
alias taskctl='supervisorctl -c '$HOME'/.config/homeless/supervisor.conf'

alias orchestra='ansible-playbook /hml/orch/topology.yml -i /hml/orch/hosts'
```

bash (Bourne Again SHell) :
---------------------------

```bash
force_color_prompt=yes

if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi
```

zsh (Zen SHell) :
-----------------

The default configuration used in ZSH is :

```zsh
CASE_SENSITIVE="true"
DISABLE_AUTO_UPDATE="true"
export UPDATE_ZSH_DAYS=13
COMPLETION_WAITING_DOTS="true"
```

Using [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh), we used the "xiong-chiamiov-plus" theme and enabled the following plugins :

* ant
* cloudapp
* compleat
* command-not-found
* coffee
* django
* docker
* encode64
* gem
* gitfast
* git-extras
* git-flow
* git-hubflow
* github
* grails
* jira
* heroku
* history
* history-substring-search
* jira
* knife
* knife_ssh
* last-working-dir
* lol
* mvn
* nanoc
* node
* npm
* nyan
* phing
* pip
* postgres
* profiles
* pyenv
* python
* rails
* rake
* rand-quote
* redis-cli
* ruby
* rvm
* screen
* supervisor
* torrent
* urltools
* vagrant
* virtualenv
* virtualenvwrapper
* web-search
* wd
