#!/bin/sh

if [ $SHL_OS=="windows" ] ; then
    export CYG_PKGs=$CYG_PKGs",python-argparse,python-avahi,python-lxml"
    #export CYG_PKGs=$CYG_PKGs",python-argparse,python-avahi,python-lxml"
fi

if [ $SHL_OS=="macosx" ] ; then
    export BREW_PKGs=$BREW_PKGs" nodejs"
fi

if [ $SHL_OS=="debian" ] ; then
    export APT_PKGs=$APT_PKGs" nodejs"
fi

export NPM_PKGs="less"

alias nodejs-install="npm install -g"

