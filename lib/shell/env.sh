#!/bin/sh

if [ $SHL_OS=="windows" ] ; then
    export CYG_PKGs=$CYG_PKGs",nano"
    export CYG_PKGs=$CYG_PKGs",bash,dash,fish,mksh,zsh"
    export CYG_PKGs=$CYG_PKGs",tree,htop,nmap"
fi

if [ $SHL_OS=="macosx" ] ; then
    export BREW_PKGs=$BREW_PKGs" mksh"
fi

if [ $SHL_OS=="debian" ] ; then
    export APT_PKGs=$APT_PKGs" mksh zsh zsh-lovers"
    export APT_PKGs=$APT_PKGs" tree htop fping nmap traceroute"
fi

export NPM_PKGs="$NPM_PKGs nsh"

alias lh='ll -h'
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

alias lsize='ls -l $1 | wc -l'

alias superctl='supervisorctl'
alias superstart='supervisorctl start'
alias superrestart='supervisorctl restart'
alias superstop='supervisorctl stop'
alias supertail='supervisorctl tail -f'

alias ssh-clear='ssh-keygen -f $HOME/.ssh/known_hosts -R '

