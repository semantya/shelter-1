#!/bin/sh

if [ $SHL_OS=="windows" ] ; then
    export CHOCO_PKGs=$CHOCO_PKGs" ruby1.9 rubygems"
fi

#if [ $SHL_OS=="macosx" ] ; then
#	export BREW_PKGs=$BREW_PKGs" nodejs"
#fi

if [ $SHL_OS=="debian" ] ; then
    export APT_PKGs=$APT_PKGs" ruby1.9.3 rubygems"
fi

export GEM_PKGs="fpm god foreman bluepill"
export GEM_PKGs="$GEM_PKGs dydra"
export GEM_PKGs="$GEM_PKGs rhc heroku af"
export GEM_PKGs="$GEM_PKGs cloudapp_api"

alias gem-install="gem install --no-rdoc --no-ri"

