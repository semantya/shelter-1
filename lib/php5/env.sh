#!/bin/sh

if [ $SHL_OS=="windows" ] ; then
    #export CYG_PKGs=$CYG_PKGs",bash,dash,fish,mksh,zsh"
fi

#if [ $SHL_OS=="macosx" ] ; then
#	export BREW_PKGs=$BREW_PKGs" nodejs"
#fi

if [ $SHL_OS=="debian" ] ; then
    export APT_PKGs="$APT_PKGs php-pear `echo php5-{gd,imap,memcache,mysql,sqlite}`"
fi

export PECL_PKGs="mongo"

export PHP5_PATH=$SHL_ROOT"/lib/php5/dist:"$SHL_ROOT"/lib/php5/dist"

alias php5-install="pecl install"

