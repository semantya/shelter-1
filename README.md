Preparing the system :
===========================

Depending on your operating system, follow the steps defined in the links below :

* [Debian & Ubuntu](https://bitbucket.org/maher-os/debian)
* [Mac OS X](https://bitbucket.org/maher-os/macosx)
* [RedHat](https://bitbucket.org/maher-os/redhat)
* [Windows 7+](https://bitbucket.org/maher-os/windows)

Installing the shelter :
========================

```bash
source /shl/boot/env/sh

shelter_os_setup
```

Becoming homless :
==================

Proceed by setting up your [Homeless](https://bitbucket.org/maher-goodies/homeless) specs, then launch a terminal :-D

Keeping up :
============

```bash
shelter update
```

Enjoy hacking your potential !-)
