#!/usr/bin/python

from shelter.shortcuts import *

#######################################################################################

class Greetings(PersistentDirective):
    def rpath(self, *args):
        return '/'.join(['/media/center']+list(args))
    
    #************************************************************************************
    
    def spath(self, *args):
        return '\\'.join(['\\\\', self.args['host']]+list(args))
    
    ###################################################################################
    
    def configure(self, *args, **kwargs):
        for k in self.SERVICEs:
            self.SERVICEs[k]['name']   = k
            self.SERVICEs[k]['remote'] = self.SERVICEs[k].get('remote', 'localhost')
        
        #for svc in self.SERVICEs.values():
        #    self.parser.add_argument('--%(cmd)s' % svc, dest='tunnel_%(name)s' % svc, action='store_true',
        #                        help="Enable remote tunneling for '%(name)s'." % svc)
        
        if hasattr(self, 'extend'):
            if callable(self.extend):
                self.extend()
    
    #************************************************************************************
    
    def ssh(self, *args, **opts):
        cmd = ['ssh']
        
        if opts.get('x11', False):
            cmd.append('-X')
        
        for svc in self.SERVICEs.values():
            if opts.get('tunnel_%(name)s' % svc, False) or opts.get('full', False):
                for prt in svc['port']:
                    cmd.append('-L')
                    cmd.append("%d:%s:%d" % (prt, svc['remote'], prt))
        
        cmd += ['%(user)s@%(host)s' % opts]
        
        if opts.get('port', None):
            cmd += ['-p', opts['port']]
        
        if opts.get('pem_file', None):
            cmd += ['-i', opts['pem_file']]
        
        return cmd

#######################################################################################

class Landing(PersistentCommandline):
    TITLE = 'The Homeless'
    CFG_PATH = '/hml/etc/shelter/hosts.yml'
    
    def default_args(self, parser):
        pass
    
    #************************************************************************************
    
    def load_config(self, *args, **kwargs):
        pass
    
    def post_save(self, *args, **kwargs):
        Nucleon.local.chdir('/hml')
        
        # Nucleon.local.chdir('git', 'commit', '-a')
    
    #************************************************************************************
    
    def before_command(self, *args, **kwargs):
        pass
    
    def after_command(self, *args, **kwargs):
        pass
    
    #************************************************************************************
    
    @property
    def current(self):
        for entry in self.cfg:
            if entry['name']==Nucleon.local.hostname:
                return entry
        
        print Nucleon.local.hostname
        
        print "\t\t=> Sorry, but this host is not configured for use with your Homeless specs."
        print "\t\t=> To include it into your configurtion, execute : `homeless init`"
        exit(1)
    
    @property
    def context(self):
        return {
            'current': self.current,
        }
    
    ###################################################################################
    
    class User(Greetings):
        HELP = "Setup the user account for better shell integration with the Shelter."
        
        SHELLs = ('bash', 'mksh', 'zsh')
        
        def extend(self, *args, **kwargs):
            self.parser.add_argument('-s', '--shell', dest='shell', type=str,
                                default=os.environ['SHELL'].split('/')[-1], help="Default shell to set for current user.")
            
            self.parser.add_argument('--home', dest='home_dir', type=str,
                                default=os.environ['HOME'], help="Home directory of the user.")
            
            self.parser.add_argument('--persist', dest='persist', action='store_true',
                                default=False, help="Persist your users locally.")
        
        def execute(self, **opts):
            if not Nucleon.local.exists(opts['home_dir']):
                if opts['persist']:
                    Nucleon.local.shell('mkdir', '-p', opts['home_dir'])
                    
                    Nucleon.local.shell('chown', os.environ['USER'], '-R', opts['home_dir'])
                else:
                    print "Home directory doesn't exist, along with persistency disabled. Exisitng ..."
                    
                    exit(1)
            
            Nucleon.local.chdir(opts['home_dir'])
            
            for shell in self.SHELLs:
                Nucleon.local.shell('ln', '-sf', '/shl/bin/env/%s' % shell, '.%src' % shell)
            
            Nucleon.local.shell('usermod', '-s', '/bin/'+shell, os.environ['USER'])
    
    #************************************************************************************
    
    class Extension(Greetings):
        HELP = "Allows you to add multiple kinds of extensions to you Shelter stack or Homeless specs, including : Jitsus, Recipes and SDK's."
        
        def extend(self, *args, **kwargs):
            self.parser.add_argument('-H', '--host', dest='host', type=str, action='append',
                                default=[], help="Hosts to extend.")
            
            self.parser.add_argument('-j', '--jitsu', dest='jitsu', type=str, action='append',
                                default=[], help="Jitsu's to use.")
            
            self.parser.add_argument('-r', '--recipes', dest='recipes', type=str, action='append',
                                default=[], help="Recipes to use.")
            
            self.parser.add_argument('-s', '--sdk', dest='sdk', type=str, action='append',
                                default=[], help="SDK's to use.")
        
        def execute(self, **opts):
            hosts = opts['hosts'] or [Nucleon.local.hostname]
            
            Nucleon.local.chdir('/hml')
            
            for ctg,mapper in dict(
                jitsu   = lambda x: 'https://bitbucket.org/maher-jitsu/%s.git' % x,
                sdk     = lambda x: 'https://bitbucket.org/maher-sdk/%s.git' % x,
                recipes = lambda x: 'https://bitbucket.org/%s/recipes.git' % x,
            ).iteritems():
                for key in opts.get(ctg, []):
                    #Nucleon.local.shell('git', 'submodule', 'add', mapper(key), '%s/%s' % (ctg, key))
                    
                    Nucleon.local.shell('git', 'clone', mapper(key), '%s/%s' % (ctg, key))
    
    #************************************************************************************
    
    class Host(Edge):
        HELP = "Harvest a host using a single SSH connections. Setup the remote user's SSH key and retrieve it along with setting your SSH key."
        
        def extend(self, *args, **kwargs):
            self.parser.add_argument('host', type=str, help="Target's hostname.")
            
            self.parser.add_argument('-i', '--identity', dest='pem_file', type=str,
                                default=None, help="PEM file to use when harvesting EC2..")
            
            self.parser.add_argument('-u', '--user', dest='user', type=str,
                                default=os.environ['USER'], help="Username to manage.")
            
            #self.parser.add_argument('--keep', dest='keeper', action='store_true',
            #                    default=False, help="Upload differential commits.")
        
        def execute(self, *args, **opts):
            script = ' ; '.join([
                'if [[ ! -d $HOME/.ssh ]]',
                'then mkdir $HOME/.ssh',
                'fi',
                'cat >>$HOME/.ssh/authorized_keys',
                'if [[ ! -f .ssh/id_rsa.pub ]]',
                'then ssh-keygen -t rsa -P "" -f $HOME/.ssh/id_rsa -q',
                'fi',
                'cat .ssh/id_rsa.pub',
            ])
            
            Nucleon.local.shell('cp', '$HOME/.ssh/discovered_keys', '$HOME/.ssh/discovered_keys.raw')
            
            Nucleon.local.shell('cat', '$HOME/.ssh/id_rsa.pub', '|', *(self.ssh(**opts)+['--', "'"+script+"'", '>>$HOME/.ssh/discovered_keys.raw']))
            
            Nucleon.local.shell('cat', '$HOME/.ssh/discovered_keys.raw', '|', 'sort', '|', 'uniq', '>', '$HOME/.ssh/discovered_keys')
            
            Nucleon.local.shell('rm', '-f', '$HOME/.ssh/discovered_keys.raw')
    
    ###################################################################################
    
    class Network(PersistentDirective):
        def execute(self, host):
            lst = [svc for svc in Nucleon.daemons.values() if svc.name in (
                'hostnet', 'hostapd', 'dhcp', 'avahi', 'ntp', 'tor', 'i2p', 'miredo'
            )]
            
            for svc in lst:
                svc.control('stop')
            
            for svc in lst:
                svc.config_daemon(
                    hostname = Nucleon.local.hostname,
                    hostfqdn = Nucleon.local.hostfqdn,
                    
                    ifaces = [
                        dict(iface='lo', inet='loopback', config={}),
                        dict(iface='eth0', inet='static', config={
                            'address':   '192.168.1.253',
                            'netmask':   '255.255.255.0',
                            'gateway':   '192.168.1.1',
                        }),
                        dict(iface='wlan0', inet='static', config={
                            'address':   '10.7.0.1',
                            'netmask':   '255.255.255.0',
                            'broadcast': '10.7.0.255',
                        }),
                        dict(iface='wlan0:0', inet='static', config={
                            'address':   '10.7.0.253',
                            'netmask':   '255.255.255.0',
                            'broadcast': '10.7.0.255',
                        }),
                    ],
                )
            
            Nucleon.local.shell('/etc/init.d/networking', 'restart')
            
            for svc in lst:
                svc.control('start')
    
    #************************************************************************************
    
    class Matrix(PersistentDirective):
        TARGETs = {
            'he': {
                4: 'http://dyn4.he.maher-pur.pl/nic/update',
                6: 'http://dyn6.he.maher-pur.pl/nic/update',
            },
            'tb': {
                4: 'http://dyn4.he.maher-pur.pl/nic/update',
            },
        }
        
        def execute(self, host):
            r = requests.get('http://freegeoip.net/json/')
            
            addr = None
            
            if r.status_code==200:
                addr = r.json()['ip']
                
                print "-> Your public IP address is : %s" % addr
            else:
                print "\t#) An error happened while getting public IP address :\n\n %s" % r.text
                
                exit()
            
            for cfg in (self.current.get('dyndns', None) or []):
                for mode,link in self.TARGETs.get(cfg['provider'], {}).iteritems():
                    if mode in cfg.get('modes', []):
                        r = None
                        
                        try:
                            r = requests.get(link, auth=(cfg['hostname'],cfg['password']))
                        except requests.exceptions.ConnectionError,ex:
                            #pass #
                            print ex
                        
                        if r:
                            if r.status_code==200:
                                print "\t-> Succefully updated host '%s' on '%s' : %s" % (cfg['hostname'], link, r.text)
                            else:
                                print "\t#) An error happened while updating host '%s' on '%s' : %s" % (cfg['hostname'], link, r.text)
            
            idx = 0
            
            for cfg in self.current.get('tunnels', []):
                if False or cfg['provider']=='he':
                    cfg['iface0'] = 'he-%(uuid)s' % cfg
                    cfg['iface1'] = 'tb-%(uuid)s' % cfg
                    
                    cfg['iface0'] = 'sit%d' % (2*idx)
                    cfg['iface1'] = 'sit%d' % (2*idx+1)
                    
                    cfg['ipv4']['local'] = addr
                    
                    for cmd in ['down']: #, 'delete']:
                        for idx in range(0, 2):
                            Nucleon.local.shell('ifconfig', cfg['iface'+str(idx)], cmd)
                    
                    r = requests.get('https://ipv4.tunnelbroker.net/nic/update?hostname=%(uuid)s' % cfg, auth=(cfg['auth']['login'],cfg['auth']['passwd']))
                    
                    if r.status_code==200:
                        print "\t-> Succefully updated host for tunnel #%s : %s" % (cfg['uuid'], r.text)
                        
                        Nucleon.local.shell('ifconfig', cfg['iface0'], 'up')
                        Nucleon.local.shell('ifconfig', cfg['iface0'], 'inet6', 'tunnel', '::'+cfg['ipv4']['remote'])
                        
                        Nucleon.local.shell('ifconfig', cfg['iface1'], 'up')
                        Nucleon.local.shell('ifconfig', cfg['iface1'], 'inet6', 'add', '%(local)s/%(prefix)s' % cfg['ipv6'])
                        Nucleon.local.shell('route', '-A', 'inet6', 'add', '::/0', 'dev', cfg['iface1'])
                        
                        print "\t-> Succefully updated tunnel #%(uuid)s on platform '%(provider)s' !" % cfg
                    else:
                        print "\t#) An error happened while updating tunnel '%s' : %s" % (cfg['uuid'], r.text)
    
    ###################################################################################
    
    class Ec2(Edge):
        HELP = "Connect to EC2."
        
        def extend(self, *args, **kwargs):
            self.parser.add_argument('-z', '--zone', dest='zone', type=str,
                                default='us-west-2', help="EC2 zone.")
            
            self.parser.add_argument('-n', '--node', dest='nodes', action='append',
                                default=[], help="Target's nodes.")
            
            self.parser.add_argument('-u', '--user', dest='user', type=str,
                                default='ubuntu', help="Username to manage.")
            
            self.parser.add_argument('-i', '--identity', dest='pem_file', type=str,
                                default='/hml/etc/creds/cloud/ec2.pem', help="PEM file to use when harvesting EC2..")
        
        def execute(self, *args, **kwargs):
            #Nucleon.local.write('~/.csshrc', "ssh_args = -i %(pem_file)s" % kwargs)
            
            hell = []
            
            for target in (kwargs.get('nodes', None) or []):
                while '.' in target:
                    target = target.replace('.', '-')
                
                kwargs.update({
                    'node': target,
                })
                
                if kwargs['zone']=='us-east-1':
                    hell += ['%(user)s@ec2-%(node)s.compute-1.amazonaws.com' % kwargs]
                else:
                    hell += ['%(user)s@ec2-%(node)s.%(zone)s.compute.amazonaws.com' % kwargs]
            
            print ' '.join(['cssh', '-C', '/hml/etc/creds/cloud/ec2.cssh'] + hell)
            
            Nucleon.local.shell('cssh', '-C', '/hml/etc/creds/cloud/ec2.cssh', *hell)
    
    ###################################################################################
    

if __name__=='__main__':
    mgr = Greetings()
    
    mgr(*sys.argv)

