#!/usr/bin/python

from shelter.shortcuts import *

#######################################################################################

class Edge(PersistentDirective):
    VOLUMEs=['Games', 'Library', 'Gallery', 'JukeBox', 'Movies', 'TV', 'Clips', 'Knowledge']
    
    SERVICEs = {
        'mysql':   dict(port=[3306],  cmd='mysql'),
        'amqp':    dict(port=[5672],  cmd='amqp'),
        'couchdb': dict(port=[5984],  cmd='couch'),
        'redis':   dict(port=[6379],  cmd='redis'),
        'elastic': dict(port=[9200],  cmd='es'),
        'mongodb': dict(port=[27000], cmd='mongo'),
        
        'maher':   dict(port=[9000+i for i in range(1, 3)], cmd='maher'),
        'nebula':  dict(port=[9010+i for i in range(1, 7)], cmd='nebula'),
        'servlet': dict(port=[9030+i for i in range(1, 7)], cmd='servlet'),
    }
    
    ###################################################################################
    
    def rpath(self, *args):
        return '/'.join(['/media/center']+list(args))
    
    #************************************************************************************
    
    def spath(self, *args):
        return '\\'.join(['\\\\', self.args['host']]+list(args))
    
    ###################################################################################
    
    def configure(self, *args, **kwargs):
        for k in self.SERVICEs:
            self.SERVICEs[k]['name']   = k
            self.SERVICEs[k]['remote'] = self.SERVICEs[k].get('remote', 'localhost')
        
        #for svc in self.SERVICEs.values():
        #    self.parser.add_argument('--%(cmd)s' % svc, dest='tunnel_%(name)s' % svc, action='store_true',
        #                        help="Enable remote tunneling for '%(name)s'." % svc)
        
        if hasattr(self, 'extend'):
            if callable(self.extend):
                self.extend()
    
    #************************************************************************************
    
    def ssh(self, *args, **opts):
        cmd = ['ssh']
        
        if opts.get('x11', False):
            cmd.append('-X')
        
        for svc in self.SERVICEs.values():
            if opts.get('tunnel_%(name)s' % svc, False) or opts.get('full', False):
                for prt in svc['port']:
                    cmd.append('-L')
                    cmd.append("%d:%s:%d" % (prt, svc['remote'], prt))
        
        cmd += ['%(user)s@%(host)s' % opts]
        
        if opts.get('port', None):
            cmd += ['-p', opts['port']]
        
        if opts.get('pem_file', None):
            cmd += ['-i', opts['pem_file']]
        
        return cmd

#######################################################################################

class Homeless(PersistentCommandline):
    TITLE = 'The Homeless'
    CFG_PATH = '/hml/etc/shelter/hosts.yml'
    
    def default_args(self, parser):
        pass
    
    #************************************************************************************
    
    def load_config(self, *args, **kwargs):
        pass
    
    def post_save(self, *args, **kwargs):
        Nucleon.local.chdir('/hml')
        
        # Nucleon.local.chdir('git', 'commit', '-a')
    
    #************************************************************************************
    
    def before_command(self, *args, **kwargs):
        pass
    
    def after_command(self, *args, **kwargs):
        pass
    
    #************************************************************************************
    
    @property
    def current(self):
        for entry in self.cfg:
            if entry['name']==Nucleon.local.hostname:
                return entry
        
        print Nucleon.local.hostname
        
        print "\t\t=> Sorry, but this host is not configured for use with your Homeless specs."
        print "\t\t=> To include it into your configurtion, execute : `homeless init`"
        exit(1)
    
    @property
    def context(self):
        return {
            'current': self.current,
        }
    
    ###################################################################################
    
    class Watch(Edge):
        HELP = "Watch out the disponibility of the remote host instead of connecting."
        
        def extend(self, *args, **kwargs):
            self.parser.add_argument('-H', '--host', dest='hosts', action='append',
                                default=[], help="Target's hostname.")
            
            self.parser.add_argument('-i', '--interval', dest='interval', type=int,
                                default=3, help="Interval of refresh (in seconds).")
        
        def execute(self, *args, **opts):
            #hosts += opts['hosts']
            
            flags = ['-s', '-e', '-u', '-l']
            
            Nucleon.local.shell('fping', '-i', str(opts['interval']), *(flags + opts['hosts']))
    
    #************************************************************************************
    
    class Exec(Edge):
        HELP = "Execute commands on remote hosts."
        
        def extend(self, *args, **kwargs):
            self.parser.add_argument('-H', '--host', dest='host', action='append',
                                default=[], help="Target's hostname.")
            
            self.parser.add_argument('-u', '--user', dest='user', type=str,
                                default=os.environ['USER'], help="Username to manage.")
            
            self.parser.add_argument('-c', '--command', dest='commands', action='append',
                                default=[], help="Commands to execute (specify as many as you need).")
        
        def execute(self, *args, **kwargs):
            Nucleon.shell(*self.ssh(*self.args))
    
    ###################################################################################
    
    class Announce(Edge):
        HELP = "Discover hosts within near-by networks."
        
        def extend(self, *args, **kwargs):
            pass
            #self.parser.add_argument('-H', '--host', dest='host', action='append',
            #                    default=[], help="Target's hostname.")
        
        #******************************************************************************
        
        def devops_tools(self, parent):
            yield Zeroconf(parent, name="Hive :: MongoDB", port=80)
        
        def collect_vhosts(self, parent):
            for vht in []:
                for fqdn in vht.fqnds:
                    yield Zeroconf(parent, name=vht.title, host=fqdn, port=80)
        
        def execute(self, *args, **kwargs):
            print "*) Announcing services in near-by networks :"
            
            lst = []
            
            for hnd in (self.devops_tools, self.collect_vhosts):
                for svc in hnd(None):
                    lst.append(svc)
            
            for svc in lst:
                print "\t-> Zeroconf service '%s' ..." % (svc.name)
                
                svc.publish()
            
            raw_input("#) Press any key to unpublish the services ...")
            
            for svc in lst:
                svc.unpublish()
    
    #************************************************************************************
    
    class Discover(Edge):
        HELP = "Discover hosts in near-by networks."
        
        def extend(self, *args, **kwargs):
            pass
            #self.parser.add_argument('-H', '--host', dest='host', action='append',
            #                    default=[], help="Target's hostname.")
        
        #******************************************************************************
        
        def execute(self, *args, **kwargs):
            reg = dict(bluez=[], zeroconf={})
            
            def handle_bluez(event, *args, **kwargs):
                reg['bluez'].append(kwargs)
            
            def handle_zeroconf(interface, protocol, name, stype, domain, flags):
                print "Found service '%s' type '%s' domain '%s' " % (name, stype, domain)
                
                if flags & avahi.LOOKUP_RESULT_LOCAL:
                    # local service, skip
                    pass
                
                server.ResolveService(interface, protocol, name, stype,
                    domain, avahi.PROTO_UNSPEC, dbus.UInt32(0),
                    reply_handler=service_resolved, error_handler=print_error
                )
                
                entry = dict(iface=interface, proto=protocol, name=name, stype=stype, domain=domain, flags=flags)
                
                reg['zerconf'] += [entry]
            
            mapping = [
                lambda : Zeroconf.Discover(lookup='_http._tcp'),
                lambda : Bluez.Discover(device=0),
            ]
            
            for hnd in mapping:
                prvd = hnd()
                
                prvd.scan()
                
                for entry in prvd:
                    print entry

if __name__=='__main__':
    mgr = Homeless()
    
    mgr(*sys.argv)

