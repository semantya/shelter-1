#!/usr/bin/nodejs

var argyle = require('argyle');

var dns = require('native-dns'), url = require('url'), httpProxy = require('http-proxy'), YAML = require('yamljs');

var WarZone = {
    nodes: {},
    /**************************************************************************/
    parse_ini: {
        cloud_dns: function (key) {
            var cfg = WarZone.cfg.cloud[key];
            
            for (var i=0 ; i<cfg.dns.length ; i++) {
                WarZone.forwarders.push(cfg.dns[i]);
            }
        },
    },
    initialize: function (cfg) {
        WarZone.cfg      = cfg;
        
        WarZone.nodes      = [];
        WarZone.forwarders = [];
        
        WarZone.parse_ini.cloud_dns();
        
        WarZone.srv      = {
            Proxy: httpProxy.createServer(
                require('connect-gzip').gzip(),
                
                function (req, res, proxy) {
                    var cnt = WarZone.contextify(req);
                    
                    for (var i=0 ; i<WarZone.rules.length ; i++) {
                        rlx = WarZone.rules[i];
                        
                        if (rlx.check(cnt)) {
                            rlx.perform(cnt, res, proxy);
                        }
                    }
                }
            ),
            DNS: dns.createServer(),
            SOCKS: null,
            mDNS: {},
        };
        
        for (i=0 ; i<WarZone.cfg['mDNS'].apps.length ; i++) {
            key = WarZone.cfg['mDNS'].apps[i];
            
            WarZone.srv['mDNS'][key] = mdns.createBrowser(mdns.tcp(key));
            
            WarZone.srv['mDNS'][key].on('serviceUp', function(svc) {
                console.log("<mDNS> : service up: ", svc);
            });
            WarZone.srv['mDNS'][key].on('serviceDown', function(svc) {
                console.log("<mDNS> : service down: ", svc);
            });
        }
        
        WarZone.srv['DNS'].on('request', function (request, response) {
            WarZone.resolve(request.question[0].name, function (kind, record) {
                switch (kind) {
                    case 'answer':
                        response.answer.push(record);
                        break;
                    case 'plus':
                    case 'extra':
                        response.additional.push(record);
                        break;
                }
            }, function (host, server, delta) {
                console.log("Finished resolving '"+host+"' using '"+server+"' ...");
                
                /*
                console.log('Finished processing request: ' + delta.toString() + 'ms');
                //*/
                response.send();
            }, true, WarZone.forwarders);
        });
    },
    /**************************************************************************/
    lunch: function () {
        WarZone.srv['DNS'].on('error', function (err, buff, req, res) {
            console.log(err.stack);
        });
        
        console.log('DNS server listeneing to 0.0.0.0:' + WarZone.cfg['DNS'].port);
        
        WarZone.srv['DNS'].serve(WarZone.cfg['DNS'].port);
        
        //######################################################################
        
        console.log('HTTP proxy listeneing to 0.0.0.0:' + WarZone.cfg['Proxy'].port);
        
        WarZone.srv['Proxy'].proxy.on('end', function (ev) {
            var cnt = WarZone.contextify(ev);
            
            console.log("Proxied : "+cnt.url);
        });
        
        WarZone.srv['Proxy'].listen(WarZone.cfg['Proxy'].port);
        
        //######################################################################
        
        console.log('Socks 5 proxy listeneing to 0.0.0.0:' + WarZone.cfg['Socks'].port);
        
        WarZone.srv['SOCKS'] = argyle(WarZone.cfg['Socks'].port, '0.0.0.0');
        
        WarZone.srv['SOCKS'].on('connected', function(req, dest) {
            var tReq = throttle(req, WarZone.cfg['Socks'].quota.up * 1024),
                tDest = throttle(dest, WarZone.cfg['Socks'].quota.down * 1024);
            
            dest.once('error', function(err) { req.end(); })
                .on('close', function() { req.end(); });
            
            tReq.on('data', function(chunk) {
                dest.write(chunk);
            });
            
            tDest.on('data', function(chunk) {
                req.write(chunk);
            });
        });
        
        //######################################################################
        
        for (i=0 ; i<WarZone.cfg['mDNS'].apps.length ; i++) {
            key = WarZone.cfg['mDNS'].apps[i];
            
            WarZone.srv['mDNS'][key].start();
        }
    },
    /**************************************************************************/
    contextify: function (req) {
        return {
            location: url.parse(req.url),
            method:   req.method,
            url:      req.url,
            headers:  req.headers,
            peer:     req.socket._peername,
            buffer:   httpProxy.buffer(req),
        };
    },
    add_rule: function (cfg) {
        rlx = new ProxyRule(WarZone.srv['Proxy'], cfg);
        
        WarZone.rules.push(rlx);
    },
    /**************************************************************************/
    add_node: function (fqdn, targets) {
        WarZone.nodes[fqdn+'.hood.'+WarZone.plateform.fqdn] = targets;
    },
    add_reflectors: function (fqdns, targets) {
        for (var i=0 ; i<fqdns.length ; i++) {
            for (var j=0 ; j<targets.length ; j++) {
                fqdn = targets[j][0] + '.' + fqdns[i];
                
                record = {
                    fqdn:   fqdn,
                    answer: [
                        dns.CNAME({
                            name:    fqdn,
                            data:    targets[j][2],
                            ttl:     WarZone.cfg['DNS'].ttl,
                        }),
                    ],
                    plus: [],
                };
                
                resp = WarZone.resolve(targets[j][2], null, null, false);
                
                for (var k=0 ; k<resp.length ; k++) {
                    for (var l=0 ; l<resp[k].answer.length ; l++) {
                        entry = resp[k].answer[l];
                        
                        record['answer'].push(entry);
                        record['plus'].push(entry);
                    }
                }
                
                WarZone.domains.push(record);
            }
        }
    },
    explore: function (host, cb_send, cb_end, recurse) {
        
    },
    resolve: function (host, cb_send, cb_end, recurse) {
        var resp = {
            'answer': [],
            'extra':  [],
        };
        
        console.log('Resolving '+host+' on : '+WarZone.domains);
        
        for (var i=0 ; i<WarZone.domains.length ; i++) {
            rec = WarZone.domains[i];
            
            if (rec.fqdn==host) {
                for (var i=0 ; i<rec.answer.length ; i++) {
                    if (cb_send) {
                        cb_send('answer', rec.answer[i]);
                    }
                    
                    resp['answer'].push(rec.answer[i]);
                }
                for (var i=0 ; i<rec.plus.length ; i++) {
                    if (cb_send) {
                        cb_send('extra', rec.plus[i]);
                    }
                    
                    resp['extra'].push(rec.plus[i]);
                }
            }
        }
        
        if (recurse.length) {
            console.log('Recursing '+host+' on : '+recurse[0]);
            
            var qst = dns.Request({
                question: dns.Question({
                    name: host,
                    type: dns.consts.NAME_TO_QTYPE.A,
                }),
                server: recurse[0].ipv4,
                timeout: 1000,
            });
            
            qst.lookup = {
                host:    host,
                when:    new Date().getTime(),
                send:    cb_send,
                end:     cb_end,
                recurse: [],
                answers: [],
            };
            
            for (var j=1 ; j<recurse.length ; j++) {
                qst.lookup.recurse.push(recurse[j]);
            }
            
            qst.on('timeout', function (err) {
                console.log("Error while resolving '"+this.lookup.host+"' :");
                console.log(err);
            });
            
            qst.on('message', function (err, resp) {
                if (this.lookup.send) {
                    for (var i = 0; i < resp.answer.length; i++) {
                        this.lookup.send('foreign', resp.answer.plus[i]);
                        
                        qst.lookup.answers.push(resp.answer.plus[i]);
                    }
                }
            });
            
            qst.on('end', function () {
                if (this.lookup.answers.length==0) {
                    WarZone.resolve(this.lookup.host, this.lookup.send, this.lookup.end, this.lookup.recurse);
                } else if (this.lookup.end) {
                    this.lookup.end(host, qst.server, (new Date().getTime()) - this.lookup.when);
                }
            });
            
            qst.send();
        }
        
        return resp;
    },
    /**************************************************************************/
    rebuild: function (reloader) {
        WarZone.domains = [];
        
        for (var i=0 ; i<WarZone.plateform.hosts.length ; i++) {
            record = {
                fqdn:   WarZone.plateform.hosts[i][0] + '.' + WarZone.plateform.fqdn,
                answer: [],
                plus:   [],
            };
            
            for (var j=1 ; j<WarZone.plateform.hosts[i].length ; j++) {
                node = WarZone.nodes[WarZone.plateform.hosts[i][j]+'.'+WarZone.plateform.fqdn];
                
                for (var k=0 ; k<node.length ; k++) {
                    record['answer'].push(dns.A({
                        name:    record.fqdn,
                        address: node[k],
                        ttl:     WarZone.cfg['DNS'].ttl,
                    }));
                }
            }
            
            WarZone.domains.push(record);
        }
        
        /**********************************************************************/
        
        WarZone.rules = [];
        
        /**********************************************************************/
        
        reloader();
    },
    /**************************************************************************/
    plateform: {
        fqdn: 'maher-pur.pl',
        hosts: [
            ['srv-cdn', ['ovh1.hood']],
        ],
    },
};

/******************************************************************************/

YAML.load('/shl/etc/cluster/homeless/network.yml', function(cfg) {
    function ProxyRule (srv, cfg) {
        this.server = srv;
        this.config = cfg;
    }
    
    ProxyRule.prototype.check = function (cnt) {
        return true;
    };
    ProxyRule.prototype.perform = function (cnt, res, proxy) {
        var msg = "Proxied '" + cnt.url + "' via ";
        
        if (this.config.proxy) {
            proxy.proxyRequest(req, res, {
                host:   cfg.fallback.addr,
                port:   cfg.fallback.port,
                buffer: cnt.buffer,
            });
            
            msg += "proxy " + cfg.fallback;
        } else if (this.config.policy) {
            if (this.config.policy=='ban') {
                msg = "Banned '" + cnt.url + "' according to the law."
            } else if (this.config.policy=='cache') {
                msg = "Retrieved '" + cnt.url + "' from local cache."
            } else if (this.config.policy=='direct') {
                msg = "Proxied '" + cnt.url + "' directly to the Matrix."
            }
        }
        
        console.log(msg);
    };
    
    /******************************************************************************/
    
    WarZone.initialize({
        DNS: {
            port: 53,
            ttl: 600,
        },
        mDNS: {
            apps: ['beacon'],
        },
        Proxy: {
            port: 3128,
            forwarder: {
                addr: 'localhost',
                port: 3128,
            },
        },
        Socks: {
            port: 1080,
            quota: {
                up:   32,
                down: 128,
            },
        },
    });
    
    /******************************************************************************/
    
    WarZone.add_node('ovh1', ['5.14.3.22']);
    
    /******************************************************************************/
    
    WarZone.rebuild(function () {
        /*
        WarZone.add_reflectors([
            'maher-ops.pl', 'hack2use.pl',
            'enochiandesigns.com', 'espacefactory.com', 'm-gali.com',
            'weare.net', 'weare.org', 'weare.mobi',
        ], [
            ['assets', dns.CNAME, 'srv-cdn.maher-ops.pl'],
            ['media',  dns.CNAME, 'srv-cdn.maher-ops.pl'],
            //['api',  dns.CNAME, 'srv-www.maher-ops.pl'],
            ['www',    dns.CNAME, 'srv-www.maher-ops.pl'],
            //['blog',   dns.CNAME, 'srv-www.maher-ops.pl'],
        ]);
        // */
        
        /**************************************************************************/
        
        WarZone.add_rule({ domain: 'bar.com',   policy: 'direct' });
        WarZone.add_rule({ domain: 'hello.com', policy: 'cache' });
        WarZone.add_rule({ domain: 'world.com', policy: 'ban' });
        
        WarZone.add_rule({ pattern: '(.*).i2p',   tunnel: 'i2p' });
        WarZone.add_rule({ pattern: '(.*).onion', tunnel: 'tor' });
    });
    
    /******************************************************************************/
    
    WarZone.lunch();
});

