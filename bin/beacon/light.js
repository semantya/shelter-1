var fs=require('fs'), sys=require('sys'), util = require('util'), url=require('url'), S=require('string'), YAML = require('js-yaml'), Bed = require('sleep');
var abstract=require('./abstract');

var ASPECTs = ['hosting','hosts'];

exports.homeless = {};

for (var i=0 ; i<ASPECTs.length ; i++) {
    exports.homeless[ASPECTs[i]] = YAML.safeLoad(fs.readFileSync('/hml/etc/shelter/'+ASPECTs[i]+'.yml', 'utf8'));
}

exports.ports = {
    DNS:  53,
    HTTP: 3184,
};

abstract.endpoints = {
    'nexus':          0,
    'apache2':     8090,
    'squid3':      3128,
    'apt-cachier': 3142,
};

abstract.hosts = [
    { owner: 'maher',    name: 'ovh1',    addr4: ['37.187.236.53'], addr6: ['2001:41d0:52:bff::35'] },
    { owner: 'maher',    name: 'ovh1',    addr4: ['192.168.1.1'],   addr6: ['::1'] },
    { owner: 'maher',    name: 'ovh1',    addr4: ['192.168.1.14'],  addr6: ['::1'] },
    { owner: 'maher',    name: 'ovh1',    addr4: ['192.168.1.17'],  addr6: ['::1'] },
    { owner: 'maher',    name: 'ovh1',    addr4: ['192.168.1.18'],  addr6: ['::1'] },
    { owner: 'scubadev', name: 'spectre', addr4: ['192.168.1.10'],  addr6: ['::1'] },
];

exports.meta = {
    daemons: ['memcache','redis','mysql','mongodb','couchdb','elastic'],
    aspects: ['hosts','backends','apps'],
    agencies: [
        { name: 'maher',    bee: 'virgil',  gateway: 'virgil',  vhosts: ['purple'] },
        { name: 'enochian', bee: 'virgil',  gateway: 'virgil',  vhosts: ['decotaz','website','factory','mariuno','xtreme'] },
        { name: 'mokatat',  bee: 'spectre', gateway: 'spectre', vhosts: ['etsbahi','mgali','roumayssa'] },
        { name: 'scubadev', bee: 'spectre', gateway: 'spectre', vhosts: ['loujayn'] },
    ],
};

exports.cfg = {
    hosts: [],
    backends: [],
    vhosts: [],
    tools: {
        'maher': {
            'authority': { targets: ['ovh2.hood.maher.zone'], },
            'mail':      { targets: ['ovh2.hood.maher.zone'], },
            'health':    { targets: ['ovh3.hood.maher.zone'], },
            
            'code':      { targets: ['ovh3.hood.maher.zone'], },
            
            'ci':        { targets: ['jenkins-homeless.rhcloud.com'], },
            'collab':    { targets: ['mgmt-samuraisquad.rhcloud.com'], },
            'insights':  { targets: ['insights-eclectic.rhcloud.com'], },
            'status':    { targets: ['mission-shinobi.rhcloud.com'], },
            'drive':     { targets: ['mantra-samuraisquad.rhcloud.com'], },
        },
    },
};

/**************************************************************************************************************/
/**************************************************************************************************************/

var dns = require('native-dns');

exports.resolve = {
    DNS: function (lookup, cb_err, cb_data, cb_end) {
        var qst = dns.Question(lookup);
        
        var fwd = dns.Request({
            question: qst,
            server: {
                address: '8.8.8.8',
                port:    53,
                type:    'udp',
            },
            timeout: 1000,
        });
        
        fwd.on('timeout', function () {
            cb_err('timeout', null);
        });
        
        fwd.on('message', function (err, resp) {
            if (err) {
                cb_err('error', err);
            } else {
                cb_data(resp);
            }
        });
        
        fwd.on('end', function () {
            cb_end();
        });
        
        fwd.on('error', function () {
            console.log("\t#) Error on '"+lookup.name+"' ... ");
            
            cb_err('unknown', null);
        });
        
        console.log("\t-> Resolving [ ",lookup.name," ] :");
        
        fwd.send();
    },
};

/**************************************************************************************************************/
/**************************************************************************************************************/
/**************************************************************************************************************/

exports.objs = {};

for (var i=0 ; i<exports.meta['aspects'].length ; i++) {
    exports.objs[exports.meta['aspects'][i]] = [];
}

for (i=0 ; i<exports.homeless['hosts'].length ; i++) {
    exports.objs.hosts.push(new abstract.Host(exports.homeless['hosts'][i]));
}

for (i=0 ; i<exports.homeless['hosting'].backends.length ; i++) {
    exports.objs.backends.push(new abstract.Backend(exports.homeless['hosting'].backends[i]));
}

for (i=0 ; i<exports.homeless['hosting'].vhosts.length ; i++) {
    exports.objs.apps.push(new abstract.Application(exports.homeless['hosting'].vhosts[i]));
}

/*
for (i=0 ; i<exports.cfg.agencies.length ; i++) {
    agn = exports.cfg.agencies[i];
    
    for (j=0 ; j<exports.cfg.daemons.length ; j++) {
        exports.cfg.backends[agn.name][exports.cfg.daemons[j]] = {
            targets: [
                agn.bee+'.hood.'+agn.name+'.zone',
                agn.bee+'.fog.'+agn.name+'.zone',
            ],
        };
    }
    
    for (j=0 ; j<agn.vhosts.length ; j++) {
        exports.cfg.vhosts[agn.name][agn.vhosts[j]] = {
            targets: [
                agn.gateway+'.hood.'+agn.name+'.zone',
                agn.gateway+'.fog.'+agn.name+'.zone',
            ],
        };
    }
}
//*/

