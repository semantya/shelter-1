function in_array (value, collection) {
    for (var i=0 ; collection.length ; i++) {
        if (collection[i]==value) {
            return true;
        }
    }
    
    return false;
}

var http = require('http'),
    httpProxy = require('http-proxy'),
    sys=require('sys'), util = require('util'), url=require('url'), S=require('string'), YAML = require('yamljs');

/**************************************************************************************************************/

exports.proxy = httpProxy.createServer({
    router: {
        'maher.rest': 'localhost:8000',
    },
});

/**************************************************************************************************************/

exports.Context = function (req, resp) {
    this.request  = req;
    this.response = resp;
    this.params   = {};
}

exports.Context.prototype.process = function () {
    this.request.host = url.parse(this.request.url).host;
    
    var target = 'squid3';
    
    if (S(this.request.host).endsWith('.mql.enochian-pur.pl')) {
        target = Light.cfg.targets['apache2'];
    } else if (S(this.request.host).endsWith('.vhost.mokatat-pur.pl')) {
        target = Light.cfg.targets['apache2'];
    } else if (in_array(this.request.host, Light.cfg.hosts['apt-cachier'])) {
        target = Light.cfg.targets['apt-cachier'];
    }
    
    this.proxy.web(req, res, {
        target: target,
    });
    
    console.log("Proxying '", this.request.host, "' through : ", target, " ...");
    
    return;
    
    // console.log(request);
    
    var cnt = {
        request:  request,
        response: response,
        fqdn:     request.question[0].name,
        route:    null,
    };
    
    for (var i=0 ; (i<exports.routes.length && cnt.route==null) ; i++) {
        cnt = exports.routes[i].detect(cnt, exports.routes[i]);
    }
    
    if (cnt.route!=null) {
        cnt.route.process(cnt);
        
        cnt.response.send();
    } else {
        console.log("\t#) Error on '"+cnt.fqdn+"' ... ");
        
        var qst = dns.Question({
          name: 'www.google.com',
          type: 'A',
        });
        
        var fwd = dns.Request({
            question: qst,
            server: {
                address: '8.8.8.8',
                port:    53,
                type:    'udp',
            },
            timeout: 1000,
        });
        
        fwd.on('timeout', function () {
            // console.log('Timeout in making request');
            console.log("\t\t#) Timeout ...");
        });
        
        fwd.on('message', function (err, resp) {
            resp.answer.forEach(function (record) {
                console.log("\t\t=> ", record.type, " : ", record.address);
                
                cnt.response.answer.push(record);
            });
        });
        
        fwd.on('end', function () {
            // console.log('Finished processing request: ' + delta.toString() + 'ms');
            
            cnt.response.send();
        });
        
        console.log("\t-> Resolving [ ",cnt.fqdn," ] :");
        
        fwd.send();
    }
};

/**************************************************************************************************************/

exports.initialize = function (Light) {
    exports.routes.push({
        detect: function (context, route) {
            var resp = context.fqdn.match( /(.+)\.onion/i );
            
            if (resp!=null) {
                context.route = route;
            }
            
            return context;
        },
        process: function (context) {
            console.log("\t-> Tor access to : ",context.url);
            
            Light.proxy.HTTP(context, Light.cfg['proxies']);
        },
    });
    
    exports.routes.push({
        detect: function (context, route) {
            var resp = context.fqdn.match( /(.+)\.(hood|fog)\.(.+)\.zone/i );
            
            if (resp!=null) {
                context.host = resp[1];
                context.kind = resp[2];
                context.organization = resp[3];
                
                context.route = route;
            }
            
            return context;
        },
        process: function (context) {
            console.log("\t-> Resolving <host>[ ",context.host," ]{ ",context.organization," }");
            
            if (exports.cfg.hosts[context.host]) {
                for (i=0 ; i<exports.cfg.hosts[context.host].addr4.length ; i++) {
                    context.response.answer.push(dns.A({
                        name: context.host+'.hood.'+context.organization+'.zone',
                        address: exports.cfg.hosts[context.host].addr4[i],
                        ttl: 600,
                    }));
                }
                for (i=0 ; i<exports.cfg.hosts[context.host].addr6.length ; i++) {
                    context.response.additional.push(dns.AAAA({
                        name: context.host+'.fog.'+context.organization+'.zone',
                        address: exports.cfg.hosts[context.host].addr6[i],
                        ttl: 600,
                    }));
                }
            }
        },
    });
    
    exports.server = http.createServer(function (req, resp) {
        var cnt = new HttpContext(req, resp);
        
        cnt.process(Light);
    });
};

exports.lunch = function (port) {
    console.log("Listening on port 0.0.0.0:"+port+" ...");
    
    exports.server.listen(port);
};

