export SHL_SDK=$SHL_SDK' google_appengine google_android google_dart'

export GAE_DIR=$SHL_ROOT/usr/sdk/google/appengine
export ANDROID_SDK_DIR=$SHL_ROOT/usr/sdk/google/android
export DART_SDK_DIR=$SHL_ROOT/usr/sdk/google/dart

alias appcfg='python $GAE_DIR/python/appcfg.py'
alias dev_appserver='python $GAE_DIR/python/dev_appserver.py'

#export SHL_PATH=$SHL_PATH':'

setup_google_appengine () {
    target=$GAE_DIR/python

    if [[ ! -d $target ]] ; then
        echo "" >>/dev/null
    fi

    target=$GAE_DIR/java

    if [[ ! -d $target ]] ; then
        echo "" >>/dev/null
    fi
}

init_google_appengine () {
    echo "" >>/dev/null
}

setup_google_android () {
    target=$ANDROID_SDK_DIR

    if [[ ! -d $target ]] ; then
        echo "" >>/dev/null
    fi
}

init_google_android () {
    echo "" >>/dev/null
}

setup_google_dart () {
    target=$DART_SDK_DIR

    if [[ ! -d $target ]] ; then
        echo "" >>/dev/null
    fi
}

init_google_dart () {
    echo "" >>/dev/null
}
