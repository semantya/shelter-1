export SHL_SDK=$SHL_SDK' cm7'

alias cm7-repo=$SHL_ROOT'/usr/sdk/cm7/tools/repo'

setup_cm7 () {
    apt-install bison build-essential curl flex git-core gnupg gperf libesd0-dev libncurses5-dev libsdl1.2-dev libwxgtk2.8-dev libxml2 libxml2-utils lzop openjdk-6-jdk openjdk-6-jre pngcrush schedtool squashfs-tools xsltproc zip zlib1g-dev

    target=$SHL_ROOT/usr/sdk/cm7

    for flavor in gingerbread ; do
        if [[ ! -d $target/releases/$flavor ]] ; then
            git clone https://github.com/android-warrior/cm7-os.git $target/releases/$flavor -b $flavor
        fi
    done
}

init_cm7 () {
    echo "" >>/dev/null
}
